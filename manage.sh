#!/bin/bash
set -e

BASEDIR=$(cd $(dirname $0); pwd )

# path
export PYTHONPATH="$BASEDIR/src:$PYTHONPATH"

# check if virtualenv exists and activate it
. $BASEDIR/virtualenv/bin/activate

export DJANGO_SETTINGS_MODULE=${DJANGO_SETTINGS_MODULE:-"tecnocolonventa.settings"}

cd src

if [ "$(which django-admin.py)" ] ; then
        django-admin.py $*
elif [ "$(which django-admin)" ] ; then
        django-admin $*
else
        echo "ERROR: couldn't locate django-admin nor django-admin.py"
        exit 1
fi
