#!/bin/bash
BASE_DIR=`dirname ${BASH_SOURCE[0]}`

command="ansible-playbook -i $BASE_DIR/hosts.ini "
skip=("requirements" "configuration" "locale")

# retrieve remote host name
if [ ! -n "$1" ]
then
    echo "Debe indicar un remote (production, testing, local)"
    exit 1
fi

if [ "$1" == 'local' ]
then
    playbook="$BASE_DIR/local_deploy.yml"
    unset skip
elif [ "$1" == '--' ]
then
    playbook="$BASE_DIR/create_project.yml"
    unset skip
else
    playbook="$BASE_DIR/remote_deploy.yml"
    variables="hosts=$1"
fi

# retrieve other options
while [[ $# > 0 ]]
do
    shift
    key="$1"

    case $key in
        -b|--branch)
            shift

            if [ -z "${1##-*}" ]
            then
                echo "Debe indicar una branch!"
                exit 1
            fi

            variables+=" branch=$1"
        ;;
        -r|--requirements)
            unset skip[0]
        ;;
        -c|--configuration)
            unset skip[1]
        ;;
        -l|--locale)
            unset skip[2]
        ;;
        --setup)
            playbook="$BASE_DIR/remote_setup.yml"
            unset skip
        ;;
        *)
            extra+=" $1"
        ;;
    esac
done

# finish up building the command
# concatenate command, playbook, variables and extra
command+=" ${playbook}"

if [ -n "$variables" ]
then
    command+=" -e '${variables}'"
fi

if [ -n "$extra" ]
then
    command+="${extra}"
fi

if [ ${#skip[@]} -ne 0 ]
then
    skip=$( IFS=,; echo "${skip[*]}" )
    command+=" --skip-tags=\"${skip}\""
fi

# run
echo "Running <${command}>..."
eval $command
