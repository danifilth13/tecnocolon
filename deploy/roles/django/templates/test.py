# -*- coding: utf-8 -*-
# Django settings for {{ project_name }} project.

# Make this unique, and don't share it with anybody. / apg -m 64
SECRET_KEY = '{{ secret_key }}'

ADMINS = (
    (u'Agustín Carrasco', 'agustin@pigmentes.com'),
    (u'Gustavo Del Guidice', 'gustavo@pigmentes.com'),
)

MANAGERS = (
    (u'Agustín Carrasco', 'agustin@pigmentes.com'),
    (u'Gustavo Del Guidice', 'gustavo@pigmentes.com'),
)

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '{{ project_name }}',
        'USER': '{{ project_name }}',
        'PASSWORD': '{{ db_pass }}',
        'HOST': 'localhost',
        'PORT': '5432',
    },
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': '/var/tmp/django_cache',
    }
}

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
STATIC_ROOT = '{{ static_path }}'
MEDIA_ROOT = '{{ media_path }}'
