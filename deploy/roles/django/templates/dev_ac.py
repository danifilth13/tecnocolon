# -*- coding: utf-8 -*-
# Django settings for fn project.

# Make this unique, and don't share it with anybody. / apg -m 64
SECRET_KEY = '{{ secret_key }}'

ADMINS = (
    (u'Agustín Carrasco', 'agustin@pigmentes.com'),
)

MANAGERS = (
    (u'Agustín Carrasco', 'agustin@pigmentes.com'),
)

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '{{ project_name }}',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    },
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}
