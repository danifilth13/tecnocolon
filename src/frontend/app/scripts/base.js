$(function () {	

	$('.center > img').centerToParent();
	$('.expand > img').resizeToParent();

	$('#pinBoot').pinterest_grid({
		no_columns: 3,
		padding_x: 10,
		padding_y: 10,
		margin_bottom: 50,
		single_column_breakpoint: 1100
	});

	/* activate the carousel */
	$("#modal-carousel").carousel({interval:false});	

})