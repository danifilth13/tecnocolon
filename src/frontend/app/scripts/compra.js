$(function() {
  // modal compra
  $('.btn-compra').click(function(e) {
    e.preventDefault();
    $this = $(this);
    $('#modalcompra').modal().find('.modal-content').load(
      $this.attr('href')
      );
    return false;
  });

  // modal comprainfo
  $('.btn-comprainfo').click(function(e) {
    e.preventDefault();
    $this = $(this);
    $('#modalcomprainfo').modal().find('.modal-content').load(
      $this.attr('href')
      );
    return false;
  });

  // submit de formuraio de compra
  $('body').on('submit', '#compra-form', function(e) {
    e.preventDefault();
    var $form = $(this);

    // desactivamos el botón de enviar
    $form.find('button[type=submit]').attr('disabled', true);

    // enviamos el formulario
    $.ajax({
      type: "post",
      url: $form.attr('action'),
      data: $form.serialize(), 
      success: function(data) {
        $('#modalcompra .modal-content').html(data);
      }
    });
    return false;
  });

  // submit de formuraio de comprainfo
  $('body').on('submit', '#comprainfo-form', function(e) {
    e.preventDefault();
    var $form = $(this);

    // desactivamos el botón de enviar
    $form.find('button[type=submit]').attr('disabled', true);

    // enviamos el formulario
    $.ajax({
      type: "post",
      url: $form.attr('action'),
      data: $form.serialize(), 
      success: function(data) {
        $('#modalcomprainfo .modal-content').html(data);
      }
    });
    return false;
  });
})
