from django import template
from django.core.urlresolvers import reverse
from django.contrib.staticfiles.templatetags import staticfiles


register = template.Library()
URL_TEMPLATE = 'http://{0}{1}'


def generate_full_url(site, url):
    return URL_TEMPLATE.format(site.domain, url)


@register.simple_tag(takes_context=True)
def full_url(context, view, *args, **kwargs):
    site = context['site']
    url = reverse(view, args=args, kwargs=kwargs)
    return generate_full_url(site, url)


@register.simple_tag(takes_context=True)
def full_static_url(context, *args, **kwargs):
    site = context['site']
    url = staticfiles.static(*args, **kwargs)
    return generate_full_url(site, url)

@register.simple_tag(takes_context=True)
def site_url(context, *args, **kwargs):
    site = context['site']
    return generate_full_url(site, '')
