import re
from htmlentitydefs import name2codepoint

from django import template
from django.core.urlresolvers import reverse, NoReverseMatch
from django.utils.numberformat import format

register = template.Library()


@register.simple_tag(takes_context=True)
def active(context, pattern_or_urlname):
    try:
        pattern = '^' + reverse(pattern_or_urlname) + '$'
    except NoReverseMatch:
        pattern = pattern_or_urlname
    path = context['request'].path
    if re.search(pattern, path):
        return 'active'
    return ''


def _replace_entity(match):
    if match.group('name') in name2codepoint:
        return unichr(name2codepoint[match.group('name')])
    else:
        return u'?'

entity_re = re.compile(r'&(?:amp;)?(?P<name>\w+);')


@register.filter
def stripentities(value):
    """Convert HTML entities to their equivalent unicode character.

    Usage:

        {% load stringfilters %}
        {{ html_text_var|striptags|entity2unicode }}

    """
    return entity_re.sub(_replace_entity, value)


@register.filter
def floatdot(value, decimal_pos=4):
    return format(value, ".", decimal_pos)

floatdot.is_safe = True

@register.filter
def ifexists(value, arg):
    return arg if value else ''

@register.filter
def ifnotexists(value, arg):
    return arg if not value else ''
