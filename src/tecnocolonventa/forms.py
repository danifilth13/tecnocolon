# -*- coding: utf-8 -*-
from django import forms
from . import models


class CompraForm(forms.ModelForm):
    class Meta:
        model = models.Compra
        fields = [
            'producto', 'nombre', 'apellido', 'email', 'telefono', 'direccion', 'cantidad', 
            'forma_pago', 'forma_entrega', 'costo_entrega', 'precio',
        ]
        widgets = {
            'producto': forms.HiddenInput,

            'nombre': forms.TextInput(
                attrs={'placeholder': u'Nombre *', 'class': 'form-control'}
            ),
            'apellido': forms.TextInput(
                attrs={'placeholder': u'Apellido *', 'class': 'form-control'}
            ),
            'email': forms.TextInput(
                attrs={'placeholder': u'E-mail *', 'class': 'form-control'}
            ),
            'telefono': forms.TextInput(
                attrs={'placeholder': u'Teléfono *', 'class': 'form-control'}
            ),
            'direccion': forms.Textarea(
                attrs={
                    'placeholder': u'Calle, Nro, Barrio, CP, Ciudad, Provincia',
                    'class': 'form-control',
                    'rows': 2
                }
            )
        }