# -*- coding: utf-8 -*-
from django.views import generic as views
from django.template import loader
from django.core import urlresolvers, mail
from django.contrib import messages
from django.contrib.sites import shortcuts as sites

from . import models, forms

class EmailSendMixin(object):
    email_template_name = None
    email_subject = None
    email_to = None
    email_from = None

    TO_ADMIN = 'admin'
    TO_MANAGER = 'manager'

    def get_email_context_data(self, **context):
        context['request'] = self.request
        context['site'] = sites.get_current_site(self.request)
        return context

    def send_email(self, email_template_name=None, email_subject=None,
                   email_to=None, email_from=None, **context):
        template_name = email_template_name or self.email_template_name
        subject = email_subject or self.email_subject
        to = email_to or self.email_to
        from_ = email_from or self.email_from

        context = self.get_email_context_data(**context)
        html_content = loader.render_to_string(
            '{0}.html'.format(template_name),
            context
        )
        plain_text_content = loader.render_to_string(
            '{0}.txt'.format(template_name),
            context
        )

        if to == self.TO_ADMIN:
            mail.mail_admins(
                subject, plain_text_content, html_message=html_content
            )
        elif to == self.TO_MANAGER:
            mail.mail_managers(
                subject, plain_text_content, html_message=html_content
            )
        else:
            mail.send_mail(
                subject, plain_text_content, from_, to,
                html_message=html_content
            )

#index = views.TemplateView.as_view(template_name='tecnocolonventa/index.html')

class ProductosListView(views.ListView):
    model = models.Producto
    context_object_name = 'productos'
    template_name='tecnocolonventa/index.html'
    queryset = models.Producto.objects.filter(activo=True)

index = ProductosListView.as_view()


class CompraView(views.CreateView, EmailSendMixin):
    model = models.Compra
    form_class = forms.CompraForm
    success_url = urlresolvers.reverse_lazy('compra_enviada')

    def get(self, *args, **kwargs):
        self.compra_context = {
            'producto': models.Producto.objects.get(
                id=self.request.GET['producto']
            ),
            'precio': models.Producto.objects.get(
                id=self.request.GET['producto']
            )
        }

        return super(CompraView, self).get(*args, **kwargs)

    def post(self, *args, **kwargs):
        self.compra_context = {
            'producto': models.Producto.objects.get(
                id=self.request.POST['producto']
            )
        }

        return super(CompraView, self).post(*args, **kwargs)

    def get_initial(self):
        return dict(self.compra_context)

    def get_context_data(self, **kwargs):
        context = super(CompraView, self).get_context_data(**kwargs)
        context.update(self.compra_context)
        return context

    def form_valid(self, form):
        compra = form.save()

        self.send_email(
            email_template_name='tecnocolonventa/email/compra',
            email_subject=u'Nueva compra desde Venta.TecnoColon.com',
            email_to=self.TO_MANAGER,
            compra=compra
        )
        messages.success(
            self.request,
            '<strong>Mensaje enviado con exito</strong>\n'
            'Nos contactaremos con usted a la brevedad.'
        )

        return super(CompraView, self).form_valid(form)

compra = CompraView.as_view(template_name='tecnocolonventa/compra_form.html')
comprainfo = CompraView.as_view(template_name='tecnocolonventa/comprainfo_form.html')

compra_enviada = views.TemplateView.as_view(
    template_name='tecnocolonventa/compra_enviado.html'
)
