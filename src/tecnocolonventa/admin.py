from django.contrib import admin

from . import models

class CaracteristicaProductoInline(admin.TabularInline):
    model = models.CaracteristicaProducto

class ProductoAdmin(admin.ModelAdmin):
    readonly_fields = ('imagen_preview',)
    list_display = ('imagen_lista', 'nombre', 'marca', 'orden', 'activo', 'precio')
    list_filter = ('marca', 'activo',)
    inlines = (
        CaracteristicaProductoInline,
    )

    def imagen_preview(self, instance):
        if instance.imagen_principal:
            return u'<img height="150px" src="{}"/>'.format(
                instance.imagen_principal.url)
        else:
            return u'<img height="150px" src=""/>'

    imagen_preview.allow_tags = True

    def imagen_lista(self, instance):
        if instance.imagen_principal:
            return u'<img height="80px" src="{}"/>'.format(
                instance.imagen_principal.url)
        else:
            return u'<img height="80px" src=""/>'                     


    imagen_lista.allow_tags = True
    imagen_lista.description = 'Imagen'

class CompraAdmin(admin.ModelAdmin):
    list_display = ('fecha_compra','nombre', 'apellido', 'producto', 'cantidad', 'precio', 'forma_entrega', 'forma_pago', 'total_compra')
    list_filter = ('producto', 'fecha_compra', 'forma_entrega')

admin.site.register(models.Producto, ProductoAdmin)
admin.site.register(models.Marca)
admin.site.register(models.FormaEntrega)
admin.site.register(models.Sucursal)
admin.site.register(models.Caracteristica)
admin.site.register(models.Compra, CompraAdmin)