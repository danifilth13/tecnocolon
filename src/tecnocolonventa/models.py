# -*- coding: utf-8 -*-
from django.db import models

FORMA_PAGO_EFECTIVO = "Efectivo"
FORMA_PAGO_BANCO = "Transferencia Bancaria"
FORMA_PAGO_MERCADOPAGO = "Mercado Pago"
FORMA_PAGO = (
    (FORMA_PAGO_EFECTIVO, u'Efectivo'),
    (FORMA_PAGO_BANCO, u'Transferencia'),
    (FORMA_PAGO_MERCADOPAGO, u'MercadoPago'),
)

class Producto(models.Model):
    nombre = models.CharField(max_length=255)
    tipo = models.CharField(max_length=255, null=True, blank=True)
    contenido = models.CharField(max_length=255, null=True, blank=True)
    descripcion_corta = models.TextField(null=True, blank=True)
    marca = models.ForeignKey('Marca', related_name='marca', null=True, blank=True)
    precio = models.DecimalField(null=True, blank=True,
        max_digits=10, decimal_places=2)
    imagen_principal = models.ImageField(null=True, blank=True,
        upload_to='productos')
    activo = models.BooleanField(default=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    orden = models.PositiveSmallIntegerField(default=0)
    caracteristicas = models.ManyToManyField(
        'Caracteristica',
        verbose_name=(u'caracteristicas'),
        through='CaracteristicaProducto',
        related_name='caracteristicas'
    )

    @property
    def nombre_completo(self):
        return u'{} {} {}' .format(self.nombre, self.tipo, self.marca)
    
    class Meta:
        ordering = ('-activo','-orden',)

    def __unicode__(self):
        return u'{} {}' .format(self.nombre, self.tipo)

class Caracteristica(models.Model):
    nombre = models.CharField(max_length=255)

    def __unicode__(self):
        return u'{}' .format(self.nombre)


class CaracteristicaProducto(models.Model):
    producto = models.ForeignKey(
        'Producto', verbose_name=(u'producto'), related_name='caracteristica_producto')
    caracteristica = models.ForeignKey('Caracteristica', verbose_name=(u'caracteristica'))
    orden = models.PositiveSmallIntegerField(
        default=0, null=True, blank=True)

    class Meta:
        ordering = ('orden',)

    def __unicode__(self):
        return u'{}'.format(self.caracteristica)


class Marca(models.Model):
    nombre = models.CharField(max_length=255)

    def __unicode__(self):
        return u'{}' .format(self.nombre)

class FormaEntrega(models.Model):
    nombre = models.CharField(max_length=255)
    descripcion = models.CharField(max_length=255)
    sucursal = models.ForeignKey('Sucursal', related_name='sucursal', null=True, blank=True)
    costo = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)

    def __unicode__(self):
        return u'{} {}' .format(self.nombre, self.descripcion)

class Compra(models.Model):
    nombre = models.CharField(max_length=250)
    apellido = models.CharField(max_length=250)
    email = models.EmailField()
    telefono = models.CharField(max_length=20)
    producto = models.ForeignKey('Producto', related_name='productos')
    cantidad = models.PositiveIntegerField(default=1)
    precio = models.DecimalField(max_digits=10, decimal_places=2)
    costo_entrega = models.DecimalField(max_digits=10, decimal_places=2)
    forma_pago = models.CharField(
        max_length=50,
        choices=FORMA_PAGO,
        default=FORMA_PAGO_MERCADOPAGO,
        verbose_name=u'forma de pago')
    forma_entrega = models.ForeignKey('FormaEntrega', related_name='forma_entrega')
    direccion = models.CharField(max_length=255, null=True, blank=True, verbose_name=u'su dirección')
    fecha_compra = models.DateTimeField(auto_now_add=True)

    @property
    def total_compra(self):
        return self.precio*self.cantidad+self.costo_entrega

    def __unicode__(self):
        return '{} {}'.format(self.nombre, self.apellido)

class Sucursal(models.Model):
    nombre = models.CharField(max_length=255)
    direccion = models.CharField(max_length=255, verbose_name=u'dirección')

    def __unicode__(self):
        return u'{}' .format(self.nombre)