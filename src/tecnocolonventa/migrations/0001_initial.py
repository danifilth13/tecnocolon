# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Caracteristica',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='CaracteristicaProducto',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('orden', models.PositiveSmallIntegerField(default=0, null=True, blank=True)),
                ('caracteristica', models.ForeignKey(verbose_name='caracteristica', to='tecnocolonventa.Caracteristica')),
            ],
            options={
                'ordering': ('orden',),
            },
        ),
        migrations.CreateModel(
            name='Compra',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=250)),
                ('apellido', models.CharField(max_length=250)),
                ('email', models.EmailField(max_length=254)),
                ('telefono', models.CharField(max_length=20)),
                ('cantidad', models.PositiveIntegerField(default=1)),
                ('precio', models.DecimalField(max_digits=10, decimal_places=2)),
                ('forma_pago', models.CharField(default=b'Mercado Pago', max_length=50, verbose_name='forma de pago', choices=[(b'Efectivo', 'Efectivo'), (b'Transferencia Bancaria', 'Transferencia'), (b'Mercado Pago', 'MercadoPago')])),
                ('direccion', models.CharField(max_length=255, null=True, verbose_name='direcci\xf3n', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='FormaEntrega',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='ImagenProducto',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('imagen', models.ImageField(upload_to=b'productos')),
                ('titulo', models.CharField(max_length=255, null=True, blank=True)),
                ('orden', models.PositiveSmallIntegerField(default=0)),
            ],
            options={
                'ordering': ('orden',),
            },
        ),
        migrations.CreateModel(
            name='Marca',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Producto',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=255)),
                ('descripcion_corta', models.TextField(null=True, blank=True)),
                ('precio', models.DecimalField(null=True, max_digits=10, decimal_places=2, blank=True)),
                ('imagen_principal', models.ImageField(null=True, upload_to=b'productos', blank=True)),
                ('activo', models.BooleanField(default=True)),
                ('fecha_creacion', models.DateTimeField(auto_now_add=True)),
                ('orden', models.PositiveSmallIntegerField(default=0)),
                ('caracteristicas', models.ManyToManyField(related_name='caracteristicas', verbose_name='caracteristicas', through='tecnocolonventa.CaracteristicaProducto', to='tecnocolonventa.Caracteristica')),
                ('marca', models.ForeignKey(related_name='marca', blank=True, to='tecnocolonventa.Marca', null=True)),
            ],
            options={
                'ordering': ('-activo', '-orden'),
            },
        ),
        migrations.CreateModel(
            name='Sucursal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=255)),
            ],
        ),
        migrations.AddField(
            model_name='imagenproducto',
            name='producto',
            field=models.ForeignKey(related_name='imagenes', to='tecnocolonventa.Producto'),
        ),
        migrations.AddField(
            model_name='compra',
            name='forma_entrega',
            field=models.ForeignKey(related_name='forma_entrega', to='tecnocolonventa.FormaEntrega'),
        ),
        migrations.AddField(
            model_name='compra',
            name='producto',
            field=models.ForeignKey(related_name='productos', to='tecnocolonventa.Producto'),
        ),
        migrations.AddField(
            model_name='compra',
            name='sucursal',
            field=models.ForeignKey(related_name='sucursal', blank=True, to='tecnocolonventa.Sucursal', null=True),
        ),
        migrations.AddField(
            model_name='caracteristicaproducto',
            name='producto',
            field=models.ForeignKey(related_name='caracteristica_producto', verbose_name='producto', to='tecnocolonventa.Producto'),
        ),
    ]
