# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tecnocolonventa', '0002_auto_20150925_1518'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='compra',
            name='sucursal',
        ),
        migrations.AddField(
            model_name='formaentrega',
            name='descripcion',
            field=models.CharField(default=1, max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='formaentrega',
            name='sucursal',
            field=models.ForeignKey(related_name='sucursal', blank=True, to='tecnocolonventa.Sucursal', null=True),
        ),
        migrations.AddField(
            model_name='sucursal',
            name='direccion',
            field=models.CharField(default=1, max_length=255, verbose_name='direcci\xf3n'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='compra',
            name='direccion',
            field=models.CharField(max_length=255, null=True, verbose_name='su direcci\xf3n', blank=True),
        ),
    ]
