# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tecnocolonventa', '0005_auto_20150925_1654'),
    ]

    operations = [
        migrations.AddField(
            model_name='producto',
            name='contenido',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
