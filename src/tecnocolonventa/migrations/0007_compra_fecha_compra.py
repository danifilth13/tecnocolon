# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('tecnocolonventa', '0006_producto_contenido'),
    ]

    operations = [
        migrations.AddField(
            model_name='compra',
            name='fecha_compra',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 25, 17, 10, 16, 806958, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
    ]
