# tecnocolon


Crear repositorio en BitBucket
www.bitbucket.org
Dueño: Pigmentes
Name: (nombre en minúscula)
Tipo de repositorio: Git
Lenguage : Python


Login con cuenta de usuario
Ir al Workspace desde la consola
Clonar proyecto
/Se crea la carpeta y el proyecto vacío


Ir a la carpeta del proyecto
$ mkdir deploy (crea el directorio deploy en el proyecto)


cp ../deploy/* -r deploy 
(Copia del proyecto deploy donde esta el script de producción a la carpeta deploy creada en el proyecto actual)


$ virtualenv virtualenv (Se instala el virtualenv)


$ . virtualenv/bin/activate (Se activa el virtualenv)


$ pip install django (Se instala Django)
v
$ django-admin startproject cagliero (Se inicia el proyecto creado)


$ mv cagliero src (Se renombra la carpeta del proyecto por scr)


$ cd src   /   $ mkdir frontend (Se crea una carpeta frontend en el src para el proyecto)


$ cd frontend/ (Se ingresa a la carpeta)


$ brunch new gh:brunch/dead-simple (Se crean los directorios y herramientas del proyecto)
$ bower install --save bootstrap-sass-official (Se instala bootstrap con bower)

$ npm install --save sass-brunch (Se instala sass con npm)


(virtualenv)~/workspace/cagliero $ copy ../conmeca/.gitignore . (ir al directorio del proyecto y copiar el archivo .gitignore de otro proyecto que son las carpetas que debe ignorar git para cuando realiza el push

ej del archivo
*.pyc
virtualenv
src/db.sqlite3
src/media
src/conmeca/private_settings.py (Se debe reemplazar por el nombre del proyecto)


Copiar manage.sh de otro proyecto y editar el nombre del directorio por le nuevo.


Copiar requirements.txt de otro proyecto, borrar todo menos django con su version correspondiente ( para ver la versión correr en la terminal pip freeze dentro del directorio del proyecto con el virtualenv activado)


$ ./manage.sh runserver 0.0.0.0:8000 (Probamos si funciona y corre el proyecto


$ apg -m 64 (Generamos claves para la conexión de bases de datos y secret key


editar archivo production.yml de la carpeta /deploy/group_vars para poner claves
ej.
deploy_target: prod
branch: master
db_pass: db_pass (Cambiar por clave generada)
secret_key: secret_key (Cambiar por clave generada)


editar archivo all.yml de la carpeta /deploy/group_vars
Definir
project_name: 'cagliero' (se define el nombre del proyecto)
gunicorn_port: 8010 (Un puerto distinto por proyecto)


$ ssh ubuntu@54.69.241.50 (Se prueba conexión al servidor)




$ git push origin master:master (Se realiza el push al servidor y se crea el branch master en el servidor) 


$ deploy/deploy.sh production --setup (se instala el proyecto en el servidor)


$ deploy/deploy.sh production -r -c (se realiza el deploy del proyecto)


$ pip install psycopg2 (Instalar PostgreSQL)


pip freeze, ver versión de psycopg2 y agregarla al requirements


Editar archivo settings.py y definir las capertas para el static
# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/
STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'
STATICFILES_DIRS = (
	os.path.join(BASE_DIR, 'frontend', 'public'),
)
STATICFILES_STORAGE = \
	'django.contrib.staticfiles.storage.ManifestStaticFilesStorage'
# Media files
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'

# Email
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
DEFAULT_FROM_EMAIL = 'desarrollo@pigmentes.com'
EMAIL_HOST_USER = 'desarrollo@pigmentes.com'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'desarrollo@pigmentes.com'
EMAIL_HOST_PASSWORD = 'desaDoll21'
EMAIL_PORT = 587
EMAIL_USE_TLS = True

# Private settings
from cagliero.private_settings import *  # noqa
Editar archivo settings.py y agregar a la variable TEMPATES = [{
       'DIRS': [
            os.path.join(BASE_DIR, 'templates'),
        ], 
(reemplazar el DIRS que esta vacio)
}]


Copiar archivo private_settings.py de otro proyeto a la carpeta src/nombreproyecto
Controlar que en el gitignore que incluya el privatesettings.

Contenido ejemplo del Private_settings



En caso de tener:
“error: Compiling of 'app/styles/site.scss' failed. file to import not found or unreadable: bootstrap”

Agregar al archivo “src/frontend/brunch-config.coffee”
La siguiente línea:

conventions:
  assets: /assets[\\/](?!javascripts[\\/]bootstrap)/
plugins:
  sass:
    options:
      includePaths: [
        'bower_components/bootstrap-sass-official/assets/stylesheets'
]


